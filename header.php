<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available （Internet Explorerで最新のレンダリングエンジンを強制的に使用する）?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) （モバイル用メタ（やった！）） ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) （アイコンとファビコン（追加の情報: （URL））） ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win （もしくは、WindowsのIE10用に/favicon.icoを設置してください） ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions（Wordpressのhead関数） ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here（Google Analyticsをここに置いてください） ?>
		<?php // end analytics ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div id="container">

			<header class="header" role="banner" itemscope itemtype="http://schema.org/WPHeader">

				<div id="inner-header" class="wrap cf">

					<?php // to use a image just replace the bloginfo('name') with your img src and remove the surrounding <p> （画像を使うためには、bloginfo('name')をあなたの画像のソースに置き換え、周りの<p>を削除してください） ?>
					<p id="logo" class="h1" itemscope itemtype="http://schema.org/Organization"><a href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></p>

					<?php // if you'd like to use the site description you can un-comment it below （サイトの説明を使いたい場合は、以下のコメントを解除してください） ?>
					<?php // bloginfo('description'); ?>


					<nav role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<?php wp_nav_menu(array(
    					         'container' => false,                           // remove nav container　（nav containerを削除）
    					         'container_class' => 'menu cf',                 // class of container (should you choose to use it)（containerのクラス（これを使うことをおすすめします））
    					         'menu' => __( 'The Main Menu', 'bonestheme' ),  // nav name（navの名前）
    					         'menu_class' => 'nav top-nav cf',               // adding custom nav class（navのカスタムクラスの追加）
    					         'theme_location' => 'main-nav',                 // where it's located in the theme（テーマの中の場所）
    					         'before' => '',                                 // before the menu（メニューの前）
        			               'after' => '',                                  // after the menu（メニューの後）
        			               'link_before' => '',                            // before each link（各リンクの前）
        			               'link_after' => '',                             // after each link（各リンクの後）
        			               'depth' => 0,                                   // limit the depth of the nav（ナビゲーションの深さの限界）
    					         'fallback_cb' => ''                             // fallback function (if there is one)（フォールバック関数 （ある場合））
						)); ?>

					</nav>

				</div>

			</header>
