# Bones
A Lightweight Wordpress Development Theme
    軽量のWordpress開発用テーマ

Bones is designed to make the life of developers easier. It's built
using HTML5 & has a strong semantic foundation.
It's constantly growing so be sure to check back often if you are a
frequent user. I'm always open to contribution. :)
    Bonesは開発ライフを簡単にするために設計されています。これはHTML5を使用して、強力でセマンティックな基盤を持って構築されています。
    Bonesは頻繁に成長しているので、もしあなたが頻繁なユーザならば、必ず頻繁に確認してください。私はいつでも改善を受け入れています。 :)

Designed by Eddie Machado
http://themble.com/bones

License: WTFPL
License URI: http://sam.zoy.org/wtfpl/
Are You Serious? Yes.

#### Special Thanks to:
Paul Irish & the HTML5 Boilerplate
Yoast for some WP functions & optimization ideas
Andrew Rogers for code optimization
David Dellanave for speed & code optimization
and several other developers. :)

#### Submit Bugs & or Fixes:
https://github.com/eddiemachado/bones/issues

To view Release & Update Notes, read the CHANGELOG.md file in the main folder.
    リリースやアップデートノートを見るためには、メインフォルダににある CHANGELOG.md を読んでください。

For more news and to see why my parents constantly ask me what I'm
doing with my life, follow me on twitter: @eddiemachado
    より詳しいニュースや、なぜ両親がたびたび私が何をしているか尋ねているのかを知るためには、twitterで私をフォローしてください。: @eddiemachado

## Helpful Tools & Links

Yeoman generator to quickly install Bones Wordpress starter theme into your Wordpress theme folder
by 0dp
    0dpによる、Bones WordpressスターターテーマをあなたのWordpressテーマフォルダに素早くインストールするためのYomanジェネレータ
https://github.com/0dp/generator-wp-bones


