<?php
/*
 * CUSTOM POST TYPE TEMPLATE
    カスタム投稿タイプテンプレート
 *
 * This is the custom post type post template. If you edit the post type name, you've got
 * to change the name of this template to reflect that name change.
    これはカスタム投稿タイプテンプレートです。
    もしあなたが投稿タイプ名を変更した場合、変更を反映させるためにこのテンプレート名も変更する必要があります。
 *
 * For Example, if your custom post type is "register_post_type( 'bookmarks')",
 * then your single template should be single-bookmarks.php
    例えば、あなたのカスタム投稿タイプが"register_post_type( 'bookmarks')"の場合、
    あなたのsingleテンプレートはsingle-bookmarks.phpであるべきです。
 *
 * Be aware that you should rename 'custom_cat' and 'custom_tag' to the appropiate custom
 * category and taxonomy slugs, or this template will not finish to load properly.
    カスタムカテゴリーとタクソノミースラッグを最適にするために、'custom_cat'と'custom_tag'　をリネームすることを注意してください、でなければ、テプレートのロードは正しく終了しません。
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-2of3 d-5of7 cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

								<header class="article-header">

									<h1 class="single-title custom-post-type-title"><?php the_title(); ?></h1>
									<p class="byline vcard"><?php
										printf( __( 'Posted <time class="updated" datetime="%1$s" itemprop="datePublished">%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'bonestheme' ), get_the_time( 'Y-m-j' ), get_the_time(get_option('date_format')), get_the_author_link( get_the_author_meta( 'ID' ) ), get_the_term_list( $post->ID, 'custom_cat', ' ', ', ', '' ) );
									?></p>

								</header>

								<section class="entry-content cf">
									<?php
										// the content (pretty self explanatory huh)
                                        コンテンツ（自明ですよね）
										the_content();

                                        /*
                                         * Link Pages is used in case you have posts that are set to break into
                                         * multiple pages. You can remove this if you don't plan on doing that.
                                            リンクページは、あなたが複数のページに挿入される投稿を持っている場合に使われます。
                                            あなたにそうする予定がない場合、これは削除することができます。　
                                         *
                                         * Also, breaking content up into multiple pages is a horrible experience,
                                         * so don't do it. While there are SOME edge cases where this is useful, it's
                                         * mostly used for people to get more ad views. It's up to you but if you want
                                         * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
                                            また、複数のページに挿入されるコンテンツは恐ろしい経験なので、しないでください。
                                            特殊なケースでは有用ですが、主に複数の広告のビューを取得するために人々に使われています。
                                            これはあなた次第ですが、もしやりたい場合、あなたは間違っているしあなたのことが嫌いです。
                                            （OK,私はまだあなたを限りなく愛しています）
                                         *
                                         * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
                                         *
                                        */
										wp_link_pages( array(
											'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
											'after'       => '</div>',
											'link_before' => '<span>',
											'link_after'  => '</span>',
										) );
									?>
								</section> <!-- end article section -->

								<footer class="article-footer">
									<p class="tags"><?php echo get_the_term_list( get_the_ID(), 'custom_tag', '<span class="tags-title">' . __( 'Custom Tags:', 'bonestheme' ) . '</span> ', ', ' ) ?></p>

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single-custom_type.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
