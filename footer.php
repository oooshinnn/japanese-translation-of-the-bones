			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)　（nav containerを削除する場合は''を入力してください（念のため、_base.scssの.footer-linksは包まない））
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)（containerのクラス（これを使うことをおすすめします））
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name（navの名前）
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class（navのカスタムクラスを追加）
    					'theme_location' => 'footer-links',             // where it's located in the theme（テーマの中の場所）
    					'before' => '',                                 // before the menu（メニューの前）
    					'after' => '',                                  // after the menu（メニューの後）
    					'link_before' => '',                            // before each link（各リンクの前）
    					'link_after' => '',                             // after each link（各リンクの後）
    					'depth' => 0,                                   // limit the depth of the nav（navの深さの限界）
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function（フォールバック関数）
						)); ?>
					</nav>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php（全てのJavaScriptライブラリはlibrary/bones.phpで読み込まれます） ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! --><!-- サイトの終わり。なんて素敵でしょう！ -->
